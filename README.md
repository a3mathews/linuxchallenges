# hostscript.sh
Takes a txt file containing MAC and IP addresses as input and creates following files  

1) nocomments.txt - removes any comments from the input file

2) macaddresses.txt - contains unique mac addresses

3) ipaddresses.txt - contains IP addresses from the input file

### Example file - hosts.real

    # IP    hostnames     MAC   # Comments
    192.168.1.250	sqlsrv2010.citi.local   00:20:3c:14:08:5b
    192.168.1.200    employees.citi.local    employees	00:20:3c:11:fa:32	# Employee intranet site
    192.168.5.32    www.intranet.citi.local intranet        00:10:4d:32:ce:22	# Citi Intranet site
    192.168.1.1	core1.citi.local	00:15:gh:10:01:ba
    192.168.5.1     route1.citi.local	00:15:aa:20:03:0a
    192.168.5.20	feed1.citi.local	00:10:3e:4f:aa:23    # Market feed 1
    192.168.1.20	feed2.citi.local	00:10:3e:3e:03:23	# Market feed 2
    192.168.5.10	cluster1.citi.local	00:20:4d:13:12:a0
    192.168.1.30	ws01-abc.cit.local	00:13:ef:12:0e:3f
    192.168.5.23    ws02-3ef.cit.local	00:12:33:aa:3e:34	# David's PC

### Running the script
./hostscrip.sh hosts.real

# gettrades.sh
Takes a log file of record updates of market data and create file tradesoutput.txt with all Trades and their TradePrice and TradeVolume

### Sample input file - opra_example_regression.log
    Regression: Record Publish: GRAR .W    <-- Symbol/Topic
    Regression: Type: Quote                <-- Record Update Type
    Regression: wMsgType | int | * | 13
    Regression: wPubId | string | * | opra_8
    Regression: wIssueSymbol | string | * | GRAR .W
    Regression: wEntitleCode | int | * | 77
    Regression: wInstrumentType | enum | * | Option
    Regression: wSessionId | char | * |
    Regression: wQualBbo | bool | | 1
    Regression: wBidPartId | string | * | W
    Regression: wBidPrice | price | * | 2.65  <-- Modified field – denoted by '*'
    Regression: wBidSize | quantity | * | 53
    Regression: wAskPrice | price | * | 2.80
    Regression: wAskSize | quantity | * | 91
    Regression: wQuoteQualifier | string | * | Normal
    Regression: wSecurityStatus | enum | * | Normal
    Regression: wAskPartId | string | * | W
    Regression: wQuoteDate | date | * | 2010/07/05
    Regression: wQuoteTime | time | * | 13:54:00.500
    Regression: wQuoteSeqNum | int | * | 13880591
    Regression: wStrikePrice | price | * | 30.00
    Regression: wCondition | char | * |
    Regression: wPartRecord | bool | | 1      <-- Unmodified field
    Regression: wConsRecord | bool | | 0
    Regression: wLineTime | time | * | 04:36:47.434
    Regression: wSrcTime | time | * | 13:54:00.500
    
### Sample output - tradesoutput.txt
    Record Publish: GEWQ .Q  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 16
    Record Publish: GEWQ .X  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 84
    Record Publish: GEWQ .A  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 16
    Record Publish: GEWQ .W  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 100
    Record Publish: GEWQ .A  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 84
    Record Publish: GEWQ .B  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 100
    Record Publish: GEWQ .X  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 100
    Record Publish: GCZQ .W  Type: Trade
    wTradePrice | price | * | 1.30
    wTradeVolume | quantity | * | 9
    Record Publish: GEWQ .N  Type: Trade
    wTradePrice | price | * | 0.25
    wTradeVolume | quantity | * | 10
    Record Publish: GEWQ .N  Type: Trade
    wTradePrice | price | * | 0.25    
    wTradeVolume | quantity | * | 20

### Running the script
./gettrades.sh opra_example_regression.log