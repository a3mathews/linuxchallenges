#/bin/bash
# input filename when running the script

# Extract all records from the lof file
sed -ne '/Regression: Record Publish/,$ p' $1 |\
# Remove the Regression: token
sed 's/Regression:[[:space:]]*//'|\
# Extract all records of Type: Trade
sed $'s/^\(Record.*\)/\f\\1/' | awk 'BEGIN {RS="\f"} /Type: Trade/ {print}' |\
# Get the required fields
awk '/Record Publish|Type: Trade|wTradePrice|wTradeVolume/'|\
# Combine first and second line of each record
sed 'N;s/\nT/\t T/' > tradesoutput.txt