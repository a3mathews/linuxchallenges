#/bin/bash
#input file when running the script
# remove comments from the hosts.real file
cat $1 | sed 's/#.*//' > nocomments.txt
# extract mac addresses (keep only unique addresses)
cat $1 | grep -o '[0-9A-Za-z]\{2\}\(:[0-9A-Za-z]\{2\}\)\{5\}'|  awk '!seen[$0]++' > macaddresses.txt
# extract IP addresses
cat $1 | grep -o '[0-9]\{1,3\}\(\.[0-9]\{1,3\}\)\{3\}' > ipaddresses.txt
